#!/bin/bash

## To convert from USB CSV file to OFX Money file.

_INFILE="$1"
_BASEFILE=$(basename $1)
_OUTFOLDER="${HOME}/tmp"

# http://pubs.opengroup.org/onlinepubs/009695399/utilities/xcu_chap02.html
#_OUTFILE="${_INFILE%.*}"
_OUTFILE="data-build.ofx"
_LINESTOBYPASS="5"

_DATE=$(date +%Y%m%d)
_BANKID="13825"
_BRANCHID="00200"
_ACCTID="04022279701"


_HEADER="\
OFXHEADER:100
DATA:OFXSGML
VERSION:102
SECURITY:NONE
ENCODING:USASCII
CHARSET:1252
COMPRESSION:NONE
OLDFILEUID:NONE
NEWFILEUID:NONE
<OFX>
<SIGNONMSGSRSV1>
<SONRS>
<STATUS>
<CODE>0
<SEVERITY>INFO
</STATUS>
<DTSERVER>${_DATE}
<LANGUAGE>FRA
<DTPROFUP>${_DATE}
<DTACCTUP>${_DATE}
</SONRS>
</SIGNONMSGSRSV1>
<BANKMSGSRSV1>
<STMTTRNRS>
<TRNUID>00
<STATUS>
<CODE>0
<SEVERITY>INFO
</STATUS>
<STMTRS>
<CURDEF>CHF
<BANKACCTFROM>
<BANKID>${_BANKID}
<BRANCHID>${_BRANCHID}
<ACCTID>${_ACCTID}
<ACCTTYPE>SAVINGS
</BANKACCTFROM>
<BANKTRANLIST>\
"

echo "${_HEADER}" > ${_OUTFOLDER}/${_OUTFILE}

_numLine=1

while read line; do
  # Get Start and Stop date from line 0
  if [ ${_numLine} -eq "1" ]; then
    _dateStart=$(echo ${line}|cut -d";" -f3)
    _dateStop=$(echo ${line}|cut -d";" -f4)

    _dtstart=$(echo ${_dateStart}|cut -d":" -f2)
    _dtstart_year=$(echo ${_dtstart}|cut -d"/" -f3)
    _dtstart_month=$(echo ${_dtstart}|cut -d"/" -f2)
    _dtstart_day=$(echo ${_dtstart}|cut -d"/" -f1)
    _dtstart="${_dtstart_year}${_dtstart_month}${_dtstart_day}"

    _dtend=$(echo ${_dateStop}|cut -d":" -f2)
    _dtend_year=$(echo ${_dtend}|cut -d"/" -f3)
    _dtend_month=$(echo ${_dtend}|cut -d"/" -f2)
    _dtend_day=$(echo ${_dtend}|cut -d"/" -f1)
    _dtend="${_dtend_year}${_dtend_month}${_dtend_day}"

    _DT="\
<DTSTART>${_dtstart}
<DTEND>${_dtend}\
"

  echo "${_DT}" >> ${_OUTFOLDER}/${_OUTFILE}
  fi

  # Get balance from line 4
  if [ ${_numLine} -eq "4" ]; then
    _balamt=$(echo ${line}|cut -d";" -f5)

  fi


  # Bypass the first _LINESTOBYPASS lines
  if [ ${_numLine} -gt "${_LINESTOBYPASS}" ]; then
    #echo "DEBUG: Line number = ${_numLine} - Line value = ${line}"
    #_date=${line%%;*}
    #_fitid=${line%%;2}
    _date=$(echo ${line}|cut -d";" -f1)
    _opNum=$(echo ${line}|cut -d";" -f2)
    _name=$(echo ${line}|cut -d";" -f3)
    _debit=$(echo ${line}|cut -d";" -f4)
    _credit=$(echo ${line}|cut -d";" -f5)
    _memo=$(echo ${line}|cut -d";" -f6)
    #echo "DEBUG: date = ${_date} - fitid = ${_fitid}"


    # Check if we have DEBIT or CREDIT
    if [[ -n ${_debit} ]]; then
      _trntype="DEBIT"
      _trnamt="${_debit}"
    else
      _trntype="CREDIT"
      _trnamt="${_credit}"
    fi

    # Get DTPOSTED
    _dtposted=$(echo ${_opNum}|cut -c9-16)

    # Get FITID
    _fitid=$(echo ${_opNum}|cut -c-32)


    _STMTTRN="\
<STMTTRN>
<TRNTYPE>${_trntype}
<DTPOSTED>${_dtposted}
<TRNAMT>${_trnamt}
<FITID>${_fitid}
<NAME>${_name}
<MEMO>${_memo}
</STMTTRN>\
"

    echo "${_STMTTRN}" >> ${_OUTFOLDER}/${_OUTFILE}

  fi


  (( _numLine++ ))
done <${_INFILE}

## Print FOOTER
FOOTER="\
</BANKTRANLIST>
<LEDGERBAL>
<BALAMT>${_balamt}
<DTASOF>${_DATE}
</LEDGERBAL>
</STMTRS>
</STMTTRNRS>
</BANKMSGSRSV1>
</OFX>\
"

echo "${FOOTER}" >> ${_OUTFOLDER}/${_OUTFILE}

exit 0
