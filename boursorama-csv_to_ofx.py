#!/usr/bin/python3

## To convert from Revolut CSV file to OFX Money file.
import csv, sys, os, re
from datetime import date, timedelta
from dateutil.parser import parse
import random

# print(sys.argv)
_infile = sys.argv[1]
_basename = os.path.basename(_infile)
_dirname = os.path.dirname(_infile)
_outfile = _dirname + '/' + _basename.replace(" ", "").replace("-", "_").replace("csv", "ofx")

_date = date.today().strftime("%Y%m%d")
_bankid = "aacf69e0e73528889ec7b1738a88f101"
_branchid = "BOURSO"
_trnamt = ''
_currency = 'EUR'
_acctid = "00040043869"



# print(_infile)
# print(_dirname + '_____' + _basename)
# print(_outfile)
# print(_date)


# Header of OFX file
_header = '''OFXHEADER:100
DATA:OFXSGML
VERSION:102
SECURITY:NONE
ENCODING:USASCII
CHARSET:1252
COMPRESSION:NONE
OLDFILEUID:NONE
NEWFILEUID:NONE
<OFX>
<SIGNONMSGSRSV1>
<SONRS>
<STATUS>
<CODE>0
<SEVERITY>INFO
</STATUS>
<DTSERVER>''' + _date + '''
<LANGUAGE>FRA
<DTPROFUP>''' + _date + '''
<DTACCTUP>''' + _date + '''
</SONRS>
</SIGNONMSGSRSV1>
<BANKMSGSRSV1>
<STMTTRNRS>
<TRNUID>00
<STATUS>
<CODE>0
<SEVERITY>INFO
</STATUS>
<STMTRS>
<CURDEF>''' + _currency + '''
<BANKACCTFROM>
<BANKID>''' + _bankid + '''
<BRANCHID>''' + _branchid + '''
<ACCTID>''' + _acctid + '''
<ACCTTYPE>SAVINGS
</BANKACCTFROM>
<BANKTRANLIST>
'''

char_to_replace = {'é': 'e',
                   'è': 'e',
                   'à': 'a'}

## Function to return a formatted line
def lineformat(_linein):
  ## Remove space from amount
  _lineout = re.sub(r"([0-9]+) ([0-9]*\,[0-9]{2})", r"\1\2", _linein)
  ## Transform some special characters
  _lineout = re.sub(r"[\"\*]", "", _lineout)
  _lineout = re.sub(r"\€", "E", _lineout)
  _lineout = re.sub(r"[éèà]", lambda x: char_to_replace[x.group(0)], _lineout)

  ### DATE PART
  _dtposted = _lineout.split(";")[1]

  ## Convert date format
  _dtposted = parse(_dtposted)
  _dtposted = _dtposted.date().strftime("%Y%m%d")

  ## Get all fields and remove space.
  _label = _lineout.split(";")[2]
  _label = re.sub(r"^\s+|\s+$", "", _label)

  _category = _lineout.split(";")[3]
  _category = re.sub(r"^\s+|\s+$", "", _category)

  _categoryParent = _lineout.split(";")[4]
  _categoryParent = re.sub(r"^\s+|\s+$", "", _categoryParent)

  _amount = _lineout.split(";")[5]
  _amount = re.sub(r"^\s+|\s+$", "", _amount)
  _amount = re.sub(r",", ".", _amount)

  _comment = _lineout.split(";")[6]
  _comment = re.sub(r"^\s+|\s+$", "", _comment)

  _accountNum = _lineout.split(";")[7]
  _accountNum = re.sub(r"^\s+|\s+$", "", _accountNum)

  _accountLabel = _lineout.split(";")[8]
  _accountLabel = re.sub(r"^\s+|\s+$", "", _accountLabel)

  _accountBalance = _lineout.split(";")[9]
  _accountBalance = re.sub(r"^\s+|\s+$", "", _accountBalance)

  return _dtposted, _label, _category, _categoryParent, _amount, _comment, _accountNum, _accountLabel, _accountBalance


## Open Revolut CSV file
file = open(_infile, 'r')
_lines = file.readlines()

## Open OFX out file
fout = open(_outfile, 'w')

## Get the start date from the last line
_dtstart = lineformat(_lines[-1])[0]
## Get the end date from the second line
_dtend = lineformat(_lines[1])[0]
## Get balance from the second line
_balamt = lineformat(_lines[1])[8]

## Footer of OFX file
_footer = '''
</BANKTRANLIST>
<LEDGERBAL>
<BALAMT>''' + _balamt + '''
<DTASOF>''' + _date + '''
</LEDGERBAL>
</STMTRS>
</STMTTRNRS>
</BANKMSGSRSV1>
</OFX>'''

## Print OFX header
# print(_header, end = '')
fout.write(_header)

## Print OFX start and end date
# print('<DTSTART>' + _dtstart)
# print('<DTEND>' + _dtend)
fout.write('<DTSTART>' + _dtstart)
fout.write('\n<DTEND>' + _dtend)

## Now for each line we print an OFX entry
_linecount = 0

for _line in _lines:
  if _linecount == 0:
    _linecount += 1
  else:
    ## Format current line
    _newline = lineformat(_line)

    ## Place all values
    _dtposted = _newline[0]
    _label = _newline[1]
    _category = _newline[2]
    _categoryParent = _newline[3]
    _amount = _newline[4]
    _comment = _newline[5]
    _accountNum = _newline[6]
    _accountLabel = _newline[7]
    _accountBalance = _newline[8]

    ## PAID IN or PAIDOUT
    if '-' in _amount:
      _trntype = "DEBIT"
      _trnamt = _amount
    else:
      _trntype = "CREDIT"
      _trnamt = '+' + _amount

    ## Format an OFX entry
    _entry = '''<STMTTRN>
<TRNTYPE>''' + _trntype + '''
<DTPOSTED>''' + _dtposted + '''
<TRNAMT>''' + _trnamt + '''
<FITID>''' + str(random.randint(1,999999)) + '''
<NAME>''' + _label + '''
<MEMO>''' '''
</STMTTRN>'''

    ## Print current entry
    # print(_entry)
    if _dtposted > _date:  ## If entry is ahead to current date, we do not log
      print('WARNING: This data is ahead to today !!!! : ' + _dtposted)
    elif '4810' in _label: ## Card entry. We do not log
      print('WARNING: This is a card entry !!!! : ' + _label)
    else:
      fout.write('\n' + _entry)
      _linecount += 1

## Print OFX footer
# print(_footer)
fout.write(_footer)
print('INFO: File is available here: ' + _outfile)

fout.close()
exit(0)