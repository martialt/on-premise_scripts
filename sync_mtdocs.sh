#!/bin/bash

DST_MOUNTING="/home/badseed/nextcloud-davfs"
SRC_MOUNTING="/home/badseed/Private"
DST_FOLDER="${DST_MOUNTING}/MTTechnology"
SRC_FOLDER="${SRC_MOUNTING}/MTUR_Technology"
LOGS_FOLDER="/home/badseed/logs"
TAB_DIRS=(
"Avoir"
"CIPAV"
"Factures"
"Frais"
"Impots"
"Location_local"
"Releve_bancaire"
"RSI"
"URSSAF"
);

## Check if Private folder is mounted
if mountpoint -q ${SRC_MOUNTING}; then
  echo "INFO: SOURCE mounting OK"

  ## Check if NextCloud folder is mounted
  if mountpoint -q ${DST_MOUNTING}; then
    echo "INFO: DESTINATION mounting OK"

    # Sync full folder
    for folder in ${TAB_DIRS[@]}; do
      # Add -c to rsync to not verify checksum file do to encrypted file
      # Add -L because webdav does not support link
      rsync -avcL --log-file=${LOGS_FOLDER}/sync.log ${SRC_FOLDER}/${folder} ${DST_FOLDER}/
    done

    # Sync specific files - subfolders
    rsync -avcL --log-file=${LOGS_FOLDER}/sync.log ${SRC_FOLDER}/archivage/*delta* ${DST_FOLDER}/archivage/
    rsync -avcL --log-file=${LOGS_FOLDER}/sync.log ${SRC_FOLDER}/Compte_annuel/2* ${DST_FOLDER}/Compte_annuel/
  fi
else
  echo "ERROR: Check your mounting"
  exit 1
fi

# Start sync


exit 0
