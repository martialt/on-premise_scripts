#!/bin/bash

SCRIPT_FOLDER="~/Private/MTUR_Technology"
ARCHIVAGE_FOLDER="archivage"
PREVIOUS_FOLDER="${ARCHIVAGE_FOLDER}/prev"
NEW_FOLDER="${ARCHIVAGE_FOLDER}/new"
DIFF_FOLDER="${ARCHIVAGE_FOLDER}/diff"
#FILE_EXPENSE01="Frais/MTUR_Technology_-_Note_de_frais_2017.ods"
#FILE_EXPENSE02="Frais/MTUR_Technology_-_Note_de_frais_2018.ods"
DATE=$(date '+%Y%m%d')
DELTA_ARCHIVE_FILE="MTUR_Technology_${DATE}_delta.zip"
FULL_ARCHIVE_FILE="MTUR_Technology_${DATE}.zip"
GDRIVE_FOLDER_ID="0B2ZNhKd2USIrUXkwMEFrOXlTRG8"

# Create a new full archive
echo "INFO: Create full archive ${FULL_ARCHIVE_FILE}"
zip -q -r -9 ${ARCHIVAGE_FOLDER}/${FULL_ARCHIVE_FILE} Avoir/ CIPAV/ Factures/ Frais/ Impots/ Location_local/ Releve_bancaire/ RSI/ URSSAF/ Compte_annuel/2*

# Find the last delta archive
previousFullArchive=$(ls -1 ${ARCHIVAGE_FOLDER}|grep -v delta|tail -2|head -1)

# Create temp folder for processing
mkdir ${PREVIOUS_FOLDER}
mkdir ${NEW_FOLDER}
mkdir ${DIFF_FOLDER}

# untar previous full archive
echo "INFO: Extract previous full archive ${previousFullArchive}"
unzip -q ${ARCHIVAGE_FOLDER}/${previousFullArchive} -d ${PREVIOUS_FOLDER}

# untar new full archive
echo "INFO: Extract new full archive ${FULL_ARCHIVE_FILE}"
unzip -q ${ARCHIVAGE_FOLDER}/${FULL_ARCHIVE_FILE} -d ${NEW_FOLDER}

echo "INFO: Create delta archive ${DELTA_ARCHIVE_FILE}"
#cd ${ARCHIVAGE_FOLDER}
#for i in $(diff -rq prev/ new/|grep new|grep -v "zip"|sed "s|Only in new/: ||g"|sed "s|Only in new/||g"|sed "s|: |/|g"|sed "s/ differ//g"|grep -v Binary); do
#  cd new
#  zip -q -r -9  ../${DELTA_ARCHIVE_FILE} $i
#  cd ..
#echo "line $i"
#done
rsync -aHxv --progress --compare-dest=/home/badseed/Private/MTUR_Technology/archivage/prev /home/badseed/Private/MTUR_Technology/archivage/new/ /home/badseed/Private/MTUR_Technology/archivage/diff/
echo "INFO: Delete empty folder"
find ${DIFF_FOLDER} -type d -empty -delete

echo "INFO: Update delta archive"
cd ${DIFF_FOLDER}
zip -q -r -9  ../${DELTA_ARCHIVE_FILE} *

# Delete temp folder
cd ../..
echo "INFO: Delete temp folder"
rm -rf ${PREVIOUS_FOLDER} ${NEW_FOLDER} ${DIFF_FOLDER}

# Upload to GDRIVE
#echo "INFO: Upload to Google Drive"
#~/bin/gdrive-linux-x64 upload ~/Private/Documents/MTUR_Technology/archivage/${DELTA_ARCHIVE_FILE} -p 0B2ZNhKd2USIrUXkwMEFrOXlTRG8
#~/bin/gdrive-linux-x64 upload ~/Private/Documents/MTUR_Technology/archivage/${FULL_ARCHIVE_FILE} -p 0B2ZNhKd2USIrUXkwMEFrOXlTRG8

exit 0
