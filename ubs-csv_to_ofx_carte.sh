#!/bin/bash

## To convert from USB CSV file to OFX Money file.

_INFILE="$1"
_BASEFILE=$(basename $1)
_OUTFOLDER="${HOME}/tmp"

# http://pubs.opengroup.org/onlinepubs/009695399/utilities/xcu_chap02.html
_OUTFILE="${_BASEFILE%%.*}.ofx"
# Bypass the first line from the CSV file
_LINESTOBYPASS="2"

_DATE=$(date +%Y%m%d)
_BANKID="03233"
_BRANCHID="3821"
_ACCTID="23060289"

# Header of OFX file
_HEADER="\
OFXHEADER:100
DATA:OFXSGML
VERSION:102
SECURITY:NONE
ENCODING:USASCII
CHARSET:1252
COMPRESSION:NONE
OLDFILEUID:NONE
NEWFILEUID:NONE
<OFX>
<SIGNONMSGSRSV1>
<SONRS>
<STATUS>
<CODE>0
<SEVERITY>INFO
</STATUS>
<DTSERVER>${_DATE}
<LANGUAGE>FRA
<DTPROFUP>${_DATE}
<DTACCTUP>${_DATE}
</SONRS>
</SIGNONMSGSRSV1>
<BANKMSGSRSV1>
<STMTTRNRS>
<TRNUID>00
<STATUS>
<CODE>0
<SEVERITY>INFO
</STATUS>
<STMTRS>
<CURDEF>CHF
<BANKACCTFROM>
<BANKID>${_BANKID}
<BRANCHID>${_BRANCHID}
<ACCTID>${_ACCTID}
<ACCTTYPE>SAVINGS
</BANKACCTFROM>
<BANKTRANLIST>\
"

echo "${_HEADER}" > ${_OUTFOLDER}/${_OUTFILE}

_numLine=1

while read line; do
  unset _currency

  # Bypass the first _LINESTOBYPASS lines
  if [ ${_numLine} -gt "${_LINESTOBYPASS}" ]; then
    _currency=$(echo ${line}|cut -d";" -f8)

    if [[ -z ${_currency}  ]]; then
      #echo "DEBUG: CURRENCY is NULL"
      continue
    fi

    #echo "DEBUG: Line number = ${_numLine} - Line value = ${line}"
    #_date=${line%%;*}
    #_fitid=${line%%;2}
    _numAccount=$(echo ${line}|cut -d";" -f1)
    _numCard=$(echo ${line}|cut -d";" -f2)
    _datePurchase=$(echo ${line}|cut -d";" -f4)
    _description1=$(echo ${line}|cut -d";" -f5|sed 's/CHE//g'|sed 'y/�éèâô/eeeao/')
    _description2=$(echo ${line}|cut -d";" -f6|sed 'y/�éèâô/eeeao/')
    _debit=$(echo ${line}|cut -d";" -f11)
    _credit=$(echo ${line}|cut -d";" -f12)
    _dateValue=$(echo ${line}|cut -d";" -f13)
    _fitid="${_DATE}${RANDOM}"

    # Only for the 2nd line
    if [ ${_numLine} -eq "3" ]; then
      #echo "DEBUG: Only 1st line"
      _dateStartDay=$(echo ${_dateValue}|cut -d"." -f1)
      _dateStartMonth=$(echo ${_dateValue}|cut -d"." -f2)
      _dateStartYear=$(echo ${_dateValue}|cut -d"." -f3)

      _dateStopDay=$(date +%d)
      _dateStopMonth=$(date +%m)
      _dateStopYear=$(date +%Y)

      _DT="\
<DTSTART>${_dateStartYear}${_dateStartMonth}${_dateStartDay}
<DTEND>${_dateStopYear}${_dateStopMonth}${_dateStopDay}\
"

      echo "${_DT}" >> ${_OUTFOLDER}/${_OUTFILE}

    fi

    # Check if we have DEBIT or CREDIT
    if [[ -n ${_debit} ]]; then
      #echo "DEBUG: debit loop"
      _trntype="DEBIT"
      _trnamt="-$(echo ${_debit}|sed 's/\./,/g'|sed "s/'//g")"
    else
      #echo "DEBUG: credit loop"
      _trntype="CREDIT"
      _trnamt="+$(echo ${_credit}|sed 's/\./,/g'|sed "s/'//g")"
    fi

    # Get DTPOSTED
    _dateValueDay=$(echo ${_dateValue}|cut -d"." -f1)
    _dateValueMonth=$(echo ${_dateValue}|cut -d"." -f2)
    _dateValueYear=$(echo ${_dateValue}|cut -d"." -f3)
    _dtposted="${_dateValueYear}${_dateValueMonth}${_dateValueDay}"



    _STMTTRN="\
<STMTTRN>
<TRNTYPE>${_trntype}
<DTPOSTED>${_dtposted}
<TRNAMT>${_trnamt}
<FITID>${_fitid}
<NAME>${_description1}
<MEMO>${_description2}
</STMTTRN>\
"

    echo "${_STMTTRN}" >> ${_OUTFOLDER}/${_OUTFILE}

  fi


  (( _numLine++ ))
done <${_INFILE}


## Get balance from last line
_lastLine=$(tail -1 ${_INFILE}|cut -d";" -f13)
_bal1=$(echo ${_lastLine}|cut -d"." -f1)
_bal2=$(echo ${_lastLine}|cut -d"." -f2|cut -c1-2)

#echo "DEBUG: _BAL1 = ${_bal1}"
#echo "DEBUG: _BAL2 = ${_bal2}"

## Print FOOTER
_FOOTER="\
</BANKTRANLIST>
<LEDGERBAL>
<BALAMT>${_bal1},${_bal2}
<DTASOF>${_DATE}
</LEDGERBAL>
</STMTRS>
</STMTTRNRS>
</BANKMSGSRSV1>
</OFX>\
"

echo "${_FOOTER}" >> ${_OUTFOLDER}/${_OUTFILE}

exit 0
