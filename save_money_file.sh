#!/bin/bash

ROOT_DIR="/home/badseed"
VM_DIR="/home/libvirt-qemu/VMs"
CAP_DATA="${VM_DIR}/Money_Sunset.qcow2"
MNT_DIR="${ROOT_DIR}/cap_docs"
MONEY_FILE="MoneySunset"

sudo modprobe nbd max_part=8
sleep 4
mkdir ${MNT_DIR}
sudo qemu-nbd -f qcow2 -c /dev/nbd0 ${CAP_DATA}
sleep 4
sudo mount.ntfs-3g /dev/nbd0p2 ${MNT_DIR}
sleep 2
#sudo cp -p "${MNT_DIR}/${MONEY_FILE}" "${ROOT_DIR}/Private/${MONEY_FILE}"
sudo rsync -av "${MNT_DIR}/*" "${ROOT_DIR}/Private/"
sudo chown badseed:badseed "${ROOT_DIR}/Private/${MONEY_FILE}*"
sudo chmod 660 "${ROOT_DIR}/Private/${MONEY_FILE}*"
sudo umount ${MNT_DIR}
sleep 2
sudo qemu-nbd -d /dev/nbd0
sleep 2
rmdir ${MNT_DIR}

exit $?
