#!/bin/bash

## To convert from UBS CSV file to OFX Money file.

_INFILE="$1"
_BASEFILE=$(basename $1)
_OUTFOLDER="${HOME}/Nextcloud/Documents/tmp"

# http://pubs.opengroup.org/onlinepubs/009695399/utilities/xcu_chap02.html
_OUTFILE="${_BASEFILE%%.*}.ofx"
# Bypass the first line from the CSV file
_LINESTOBYPASS="1"

_DATE=$(date +%Y%m%d)
_BANKID="00243"
_BRANCHID="0243"
_ACCTID="00229839.40F"

# Header of OFX file
_HEADER="\
OFXHEADER:100
DATA:OFXSGML
VERSION:102
SECURITY:NONE
ENCODING:USASCII
CHARSET:1252
COMPRESSION:NONE
OLDFILEUID:NONE
NEWFILEUID:NONE
<OFX>
<SIGNONMSGSRSV1>
<SONRS>
<STATUS>
<CODE>0
<SEVERITY>INFO
</STATUS>
<DTSERVER>${_DATE}
<LANGUAGE>FRA
<DTPROFUP>${_DATE}
<DTACCTUP>${_DATE}
</SONRS>
</SIGNONMSGSRSV1>
<BANKMSGSRSV1>
<STMTTRNRS>
<TRNUID>00
<STATUS>
<CODE>0
<SEVERITY>INFO
</STATUS>
<STMTRS>
<CURDEF>CHF
<BANKACCTFROM>
<BANKID>${_BANKID}
<BRANCHID>${_BRANCHID}
<ACCTID>${_ACCTID}
<ACCTTYPE>SAVINGS
</BANKACCTFROM>
<BANKTRANLIST>\
"

echo "${_HEADER}" > ${_OUTFOLDER}/${_OUTFILE}

_numLine=1

while read line; do
  unset _currency

  # Bypass the first _LINESTOBYPASS lines
  if [ ${_numLine} -gt "${_LINESTOBYPASS}" ]; then
    _currency=$(echo ${line}|cut -d";" -f6)

    if [[ -z ${_currency}  ]]; then
      #echo "DEBUG: CURRENCY is NULL"
      continue
    fi

    #echo "DEBUG: Line number = ${_numLine} - Line value = ${line}"
    #_date=${line%%;*}
    #_fitid=${line%%;2}
    _account=$(echo ${line}|cut -d";" -f2)
    _dateStart=$(echo ${line}|cut -d";" -f7)
    _dateStop=$(echo ${line}|cut -d";" -f8)
    _dateValue=$(echo ${line}|cut -d";" -f12)
    _description1=$(echo ${line}|cut -d";" -f13|sed 'y/&éèâô/-eeao/')
    _description2=$(echo ${line}|cut -d";" -f14|sed 'y/&éèâô/-eeao/')
    _description3=$(echo ${line}|cut -d";" -f15|sed 'y/&éèâô/-eeao/')
    _fitid=$(echo ${line}|cut -d";" -f16)
    _hidden_field=$(echo ${line}|cut -d";" -f18|rev|cut -c8-|rev)
    _debit=$(echo ${line}|cut -d";" -f19)
    _credit=$(echo ${line}|cut -d";" -f20)
    _solde=$(echo ${line}|cut -d";" -f21)

    # Only for the 2nd line
    if [ ${_numLine} -eq "2" ]; then
      #echo "DEBUG: Only 1st line"
      _dateStartDay=$(echo ${_dateStart}|cut -d"." -f1)
      _dateStartMonth=$(echo ${_dateStart}|cut -d"." -f2)
      _dateStartYear=$(echo ${_dateStart}|cut -d"." -f3)

      _dateStopDay=$(echo ${_dateStop}|cut -d"." -f1)
      _dateStopMonth=$(echo ${_dateStop}|cut -d"." -f2)
      _dateStopYear=$(echo ${_dateStop}|cut -d"." -f3)

      _DT="\
<DTSTART>${_dateStartYear}${_dateStartMonth}${_dateStartDay}
<DTEND>${_dateStopYear}${_dateStopMonth}${_dateStopDay}\
"

      echo "${_DT}" >> ${_OUTFOLDER}/${_OUTFILE}
    fi

    #echo "DEBUG: _description1=${_description1}"

    # Description shaping
    if [[ $(echo ${_description1} | egrep "Paiement carte de debit|Paiement V PAY|Retrait Bancomat|CARTE") ]]; then
      #echo "DEBUG: if condition OK"
      #echo "DEBUG: _description3=${_description3}"
      #echo
      _name=$(echo ${_description3}|cut -d"," -f1)
      _memo=$(echo ${_description3}|cut -d"," -f2-)
    else
      _name=${_description2}
      _memo=${_description3}
    fi

    # Force field if null
    if [[ -z ${_name} ]]; then
      _name="To define"
      _memo="Entry is not yet taken in account"
    fi
    #echo "DEBUG: _name=${_name}"
    # Force field if null
    #if [[ ! -n ${_description2} ]]; then
    #  _description2="UBS"
    #fi

    # Check hidden field
    if [[ -n ${_hidden_field} ]]; then
      _trnamt="$(echo ${_hidden_field}|sed 's/\./,/g'|sed "s/'//g")"
      # Check if hidden field is debit or credit
      if [[ $(echo ${_hidden_field}|grep "-" ) ]]; then
        _trntype="DEBIT"
      else
        _trntype="CREDIT"
      fi
    else
      # Check if we have DEBIT or CREDIT
      if [[ -n ${_debit} ]]; then
        #echo "DEBUG: debit loop"
        _trntype="DEBIT"
        _trnamt="-$(echo ${_debit}|sed "s/'//g")"
      else
        #echo "DEBUG: credit loop"
        _trntype="CREDIT"
        _trnamt="+$(echo ${_credit}|sed "s/'//g")"
      fi
    fi

    # Get DTPOSTED
    _dateValueDay=$(echo ${_dateValue}|cut -d"." -f1)
    _dateValueMonth=$(echo ${_dateValue}|cut -d"." -f2)
    _dateValueYear=$(echo ${_dateValue}|cut -d"." -f3)
    _dtposted="${_dateValueYear}${_dateValueMonth}${_dateValueDay}"



    _STMTTRN="\
<STMTTRN>
<TRNTYPE>${_trntype}
<DTPOSTED>${_dtposted}
<TRNAMT>${_trnamt}
<FITID>${_fitid}
<NAME>${_name}
<MEMO>${_memo}
</STMTTRN>\
"
    echo "${_STMTTRN}" >> ${_OUTFOLDER}/${_OUTFILE}
  fi


  (( _numLine++ ))
done <${_INFILE}


## Get balance from last line
_lastLine=$(tail -1 ${_INFILE}|cut -d";" -f1)
_bal1=$(echo ${_lastLine}|cut -d"." -f1)
_bal2=$(echo ${_lastLine}|cut -d"." -f2|cut -c1-2)

#echo "DEBUG: _BAL1 = ${_bal1}"
#echo "DEBUG: _BAL2 = ${_bal2}"

## Print FOOTER
_FOOTER="\
</BANKTRANLIST>
<LEDGERBAL>
<BALAMT>${_bal1},${_bal2}
<DTASOF>${_DATE}
</LEDGERBAL>
</STMTRS>
</STMTTRNRS>
</BANKMSGSRSV1>
</OFX>\
"

echo "${_FOOTER}" >> ${_OUTFOLDER}/${_OUTFILE}

exit 0
